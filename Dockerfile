FROM python:3
# a lot has been copied from https://github.com/Goldmund-Wyldebeast-Wunderliebe/docker-python-3.6.0-jessie-nodejs/blob/master/Dockerfile


RUN apt-get update && apt-get upgrade -y && apt-get install -y \
        build-essential \
        ca-certificates \
        curl \
        git \
        libbz2-dev \
        libc6-dev \
        libffi-dev \
        libgdbm3 \
        libicu-dev \
        libgdbm3 \
        libjpeg62 \
        libjpeg62-turbo-dev \
        libsqlite3-0 \
        libssl-dev \
        libxml2 \
        libxml2-dev \
        libxmlsec1 \
        libxmlsec1-dev \
        libxslt1-dev \
        libxslt1.1 \
        locales \
        python-dev \
        python-pip \
        python-setuptools \
        rsync \
        sudo \
        xvfb \
        default-jre \
        expect


# install "virtualenv", since the vast majority of users of this image will want it
RUN pip install --no-cache-dir virtualenv

RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN apt-get install -y nodejs

RUN npm install -g serverless
RUN npm install -g webpack
RUN pip install awscli --ignore-installed six
RUN pip install boto3 moto pytest pytest-html pytest
RUN pip install git+https://github.com/Projectplace/pytest-tags
